# CHANGELOG



## v0.2.1 (2024-03-20)

### Fix

* fix(deploy): add deploy pipeline ([`b3d8cca`](https://gitlab.com/jlcbandiera/my-package/-/commit/b3d8ccad2091b2a02781e9c4b1467fd015bf8f47))


## v0.2.0 (2024-03-20)

### Feature

* feat(addition): add addition feature ([`10a1a37`](https://gitlab.com/jlcbandiera/my-package/-/commit/10a1a3776aa5dee3e928fcbc3aa305eb6b2b3cf7))


## v0.1.0 (2024-03-20)

### Feature

* feat(sr): add semantic release cd pipeline ([`3de4eaf`](https://gitlab.com/jlcbandiera/my-package/-/commit/3de4eaf8d0645001aa9822872ee353637e1c5ff1))

### Unknown

* add semantic_release config ([`7a043cf`](https://gitlab.com/jlcbandiera/my-package/-/commit/7a043cfaf31f899cad96f8541d1aa126cff8cb4f))

* Add new pyproject.toml to init poetry ([`46f1003`](https://gitlab.com/jlcbandiera/my-package/-/commit/46f100308e0e7dfd7b0d3c7942946b0ac6b94f5b))

* Update .gitlab-ci.yml file ([`785f3e9`](https://gitlab.com/jlcbandiera/my-package/-/commit/785f3e9cdc8a25cd7bf25fa49f396acc6851bb96))

* Add first pipeline ([`059f378`](https://gitlab.com/jlcbandiera/my-package/-/commit/059f378eef8c078194af89b3b94a1700a2c7c251))

* Initial commit ([`5f2255f`](https://gitlab.com/jlcbandiera/my-package/-/commit/5f2255f9f0eb238af2abd74d9b6424038afec458))
